# views.py
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import ListView, DetailView
from django.core.urlresolvers import reverse_lazy
from apps.posts.models import Post
# import the logging library
import logging
# from django.shortcuts import render, redirect

# Get an instance of a logger
logger = logging.getLogger(__name__)


class PostCreate(CreateView):
    model = Post
    fields = ['title', 'body', 'photo']
    success_url = '/post/add/'


class PostUpdate(UpdateView):
    model = Post
    fields = ['title', 'body']


class PostDelete(DeleteView):
    model = Post
    # success_url = reverse_lazy('post-list')


class PostList(ListView):
    queryset = Post.objects.all().order_by('-created')
    # model = Post


class PostDetail(DetailView):
    model = Post

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(PostDetail, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['post_list'] = Post.objects.all()
        return context
