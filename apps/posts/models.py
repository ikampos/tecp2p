from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField


class Post(models.Model):

    title = models.CharField(max_length=60)
    body = models.TextField()
    # image = models.ImageField(upload_to = 'uploads/images/', blank=True, null=True)
    photo = ThumbnailerImageField(
        upload_to='uploads/images/', blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.title
