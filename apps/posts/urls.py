# urls.py
from django.conf.urls import patterns, url
from apps.posts.views import PostCreate, PostUpdate, PostDelete, PostList, PostDetail

urlpatterns = patterns('',
                       # ...
                       url(r'post/add/$', PostCreate.as_view(), name='post_add'),
                       url(r'post/(?P<pk>\d+)/$',
                           PostDetail.as_view(), name='post_view'),
                       url(r'post/(?P<pk>\d+)/update/$',
                           PostUpdate.as_view(), name='post_update'),
                       url(r'post/(?P<pk>\d+)/delete/$',
                           PostDelete.as_view(), name='post_delete'),
                       url(r'post/list/$', PostList.as_view()),
                       )
